const path = require('path');

module.exports = {
  components: path.resolve(__dirname, '../src/components/'),
  views: path.resolve(__dirname, '../src/views/'),
  styles: path.resolve(__dirname, '../src/styles/'),
  stores: path.resolve(__dirname, '../src/stores/'),
}