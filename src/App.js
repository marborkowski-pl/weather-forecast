import React, { Component } from 'react';
import { inject } from 'mobx-react';
import Background from 'components/background';
import Details from 'views/details';
import Fetch from 'components/fetch';
import './App.css';

const services = [
  {
    id: 'info1',
    url: 'http://api.openweathermap.org/data/2.5/forecast?q=Warsaw,PL&appId=c495c911d5c3ed4e7593c34d142bb537'
  },
  {
    id: 'info2',
    url: 'http://api.openweathermap.org/data/2.5/forecast?q=Katowice,PL&appId=c495c911d5c3ed4e7593c34d142bb537'
  }
];

@inject('weatherStore')
class App extends Component {
  _onLoad = (data) => {
    this.props.weatherStore.setWeather(data[0].jsonBody);
    console.log(data);
  }

  render() {
    return (
      <div className="App">
        <Fetch
          onLoad={this._onLoad}
          services={services}
          >
          <Background />
          <Details />
        </Fetch>
      </div>
    );
  }
}

export default App;
