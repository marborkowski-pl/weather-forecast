import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Fetch extends Component {

  state = {
    loaded: false
  }

  componentDidMount() {
    this.loadAll();
  }

  loadAll = () => {
    const services = [];
    this.props.services.forEach(element => {
      services.push(new Promise((resolve, reject) => {
        fetch(element.url)
          .then(response => response.json())
          .then(json => resolve({ id: element.id, jsonBody: json }))
          .catch(() => reject(null));
      }));
    });

    Promise.all(services.map(p => p.catch(e => e)))
      .then(results => {
        this.props.onLoad(results);
        this.setState({
          loaded: true
        });
      })
      .catch(e => {
        this.props.onLoad([]);
        this.setState({
          loaded: true
        });
      });
  }

  render() {
    if (!this.state.loaded) {
      return (<span>Loading....</span>);
    }

    return this.props.children;
  }
}

Fetch.propTypes = {
  children: PropTypes.any,
  onLoad: PropTypes.func,
  services: PropTypes.arrayOf(PropTypes.object)
}


Fetch.defaultProps = {
  onLoad: () => {},
  services: []
}
export default Fetch;