import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import className from 'classnames';
import './assets/responsive-background.css';
import './background.css';

@inject('weatherStore')
@observer
class Background extends Component {

  render() {
    const {
      weather
    } = this.props.weatherStore;

    const css = className({
      background: true,
      responsive: true,
      [weather.list[0].weather[0].main.toLowerCase()]: true
    });

    return (
      <div className={css} />
    );
  }
}

export default Background;