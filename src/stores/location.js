// navigator.geolocation.getCurrentPosition(showPosition

import { observable, action } from 'mobx';

class LocationStore {

  @observable city = 'Warsaw';
  @observable cords = null;

  @action initOnce() {
    if (navigator.geolocation && !this.cords) {
      navigator.geolocation.getCurrentPosition(
        position => {
          console.log(position);
        },
        error => {
          if (error.code === error.PERMISSION_DENIED) {
            console.log("you denied me :-(");
          }
        }
      );
    }
  }
  
  /*
  @computed get someComputedValueHere() {
    return 1;
  }
  */
}

export default new LocationStore();