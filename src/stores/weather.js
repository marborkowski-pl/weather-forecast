import { observable, action, /*computed*/ } from 'mobx';
//const DeviceDetector = require('device-detector');

class WeatherStore {
  //@observable device = DeviceDetector.parse(window.navigator.userAgent);

  @observable weather = {};

  @action setWeather(weather) {
    this.weather = weather;
  }
  
  /*
  @computed get someComputedValueHere() {
    return 1;
  }
  */
}

export default new WeatherStore();