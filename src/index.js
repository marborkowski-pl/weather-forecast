import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'mobx-react';
import registerServiceWorker from './registerServiceWorker';
import App from './App';
import weatherStore from './stores/weather';
import locationStore from './stores/location';
import 'bootstrap/dist/css/bootstrap-grid.css';
import './index.css';

const stores = {
  weatherStore,
  locationStore
};

ReactDOM.render(
  <Provider {...stores}>
    <App />
  </Provider>,
  document.getElementById('root')
);

registerServiceWorker();
